# PROJECT TONTINE

## Requirements

For development, you will only need Node.js installed on your environement.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v8.12.0

    $ npm --version
    6.4.1

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://gitlab.com/digiserv/tontine-front.git
    $ cd tontine-front
    $ npm install

### Configure app

Rename `.env.sample` to `.env` then fill its values with appropriate strings.


## Start & watch

    $ npm start
    
## Run tests

    $ npm test
    
## Simple build for production

    $ npm run build
    
## Serve build app
    $ npx serve -s build
    
---

## Languages & tools

- [Styled Components](https://www.styled-components.com/docs/basics#getting-started)

- [Progressive Web App](https://gitlab.com/digiserv/tontine-front/wikis/Progressive-Web-Apps)

- [ReactJS](https://gitlab.com/digiserv/tontine-front/wikis/ReactJS-Workshop)
