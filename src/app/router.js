import React from 'react';

import { BrowserRouter, Switch, Route } from 'react-router-dom';

import AppProvider from './AppProvider';

import Login from '../authentication/LoginForm.container';
import Home from '../tontine/TontineList.container';

const Router = () => (
  <AppProvider>
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/home" exact component={Home} />
      </Switch>
    </BrowserRouter>
  </AppProvider>
);

export default Router;
