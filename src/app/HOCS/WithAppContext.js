import React from 'react';
import { AppContext } from '../AppProvider';

export const withAppContext = Component => {
  return props => {
    return (
      <AppContext.Consumer>
        {state => <Component {...props} context={state} />}
      </AppContext.Consumer>
    );
  };
};
