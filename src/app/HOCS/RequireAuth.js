import React, { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { withAppContext } from './WithAppContext';

class RequireAuthHOC extends Component {
  render() {
    const {
      component: Component,
      context: {
        state: { isLoggedIn }
      },
      ...rest
    } = this.props;
    return (
      <Route
        {...rest}
        render={props =>
          isLoggedIn ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: '/',
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  }
}

export default withAppContext(RequireAuthHOC);
