import React, { Component } from 'react';

export const AppContext = React.createContext('app');

class AppProvider extends Component {
  state = {
    isLoggedIn: false,
    key: 'value'
  };

  onLogin = () => {
    this.setState({ isLoggedIn: true });
  };

  onLogout = () => {
    this.setState({ isLoggedIn: false });
  };

  render() {
    return (
      <AppContext.Provider
        value={{
          state: this.state,
          onLogin: this.onLogin,
          onLogout: this.onLogout
        }}
      >
        {this.props.children}
      </AppContext.Provider>
    );
  }
}

export default AppProvider;
