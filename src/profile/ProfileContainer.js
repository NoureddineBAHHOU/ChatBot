import React, { Component } from 'react';
import UserInfo from './UserInfo';

class ProfileContainer extends Component {
  render() {
    return <UserInfo />;
  }
}

export default ProfileContainer;
