import Users from './users.json';

class AuthService {
  /**
   * Login service
   * @param {string} phoneNumber
   * @param {string} password
   * @return {object|null}
   */
  async login(params) {
    const response = await asyncFind(params);
    return response;
  }
}

export default AuthService;

function asyncFind({ phoneNumber, password }) {
  const delayTime = 1000;
  return new Promise(resolve => {
    setTimeout(() => {
      const user = Users.find(u => {
        return u.phoneNumber === phoneNumber && u.password === password;
      });
      if (user) {
        resolve({ user });
      } else {
        resolve({ message: 'Something is wrong ' });
      }
    }, delayTime);
  });
}
