import React from 'react';

const TontineList = ({ tontines }) => (
  <div>
    {tontines.map(tontine => (
      <div key={tontine.id}>
        <p>{tontine.title}</p>
        <p>{tontine.userId}</p>
        <p />
      </div>
    ))}
  </div>
);

export default TontineList;
