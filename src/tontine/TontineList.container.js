import React from 'react';
import TontineList from './TontineList';
import TontineService from '../api/TontineService';
import { Loader } from '../authentication/LoginForm';

class TontineListContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      tontines: [],
      isLoading: false
    };
    this.tontineService = new TontineService();
  }

  async componentDidMount() {
    this.setState({ isLoading: true });
    const tontines = await this.tontineService.fetchTontines();
    this.setState({ tontines, isLoading: false });
  }

  render() {
    let content;
    if (this.state.isLoading) {
      content = <Loader />;
    } else {
      content = <TontineList tontines={this.state.tontines} />;
    }
    return <React.Fragment>{content}</React.Fragment>;
  }
}

export default TontineListContainer;
