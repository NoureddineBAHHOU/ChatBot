import styled from 'styled-components';

const Wrapper = styled.div`
  max-width: 450px;
  background-color: #dba98b;
  padding: 26px 16px;
  border-radius: 6px;
  box-shadow: 0 3px 3px #dba98b;
`;

const FormContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;
  color: #fafafa;
`;

const FormLabel = styled.label`
  font-family: sans-serif;
  font-size: 14px;
  font-weight: 700;
`;

const FormField = styled.input`
  background-color: #ffffff;
  border: none;
  padding: 13px 20px;
  border-radius: 6px;
`;

const Button = styled.button`
  padding: 8;
  background-color: #fafafa;
  text-align: center;
  color: #dba98b;
  font-size: 15px;
  border: none;
  border-radius: 6px;
  box-shadow: 0 2px 2px #fafafa;
`;

export { Wrapper, FormContainer, FormLabel, FormField, Button };
