import React from 'react';
import {
  Wrapper,
  FormContainer,
  FormLabel,
  FormField,
  Button
} from './LoginForm.style';

const LoginForm = ({
  phoneNumber,
  password,
  isLoading,
  error,
  onPhoneNumberChange,
  onPasswordChange,
  onButtonClick
}) => (
  <Wrapper>
    <FormContainer>
      <FormLabel>PhoneNumber</FormLabel>
      <FormField
        type="tel"
        name="phoneNumber"
        value={phoneNumber}
        placeholder="Phone Number"
        onChange={onPhoneNumberChange}
      />
    </FormContainer>

    <FormContainer>
      <FormLabel>Password</FormLabel>
      <FormField
        type="password"
        name="*******"
        value={password}
        placeholder="Phone Number"
        onChange={onPasswordChange}
      />
    </FormContainer>

    <Button disabled={isLoading} onClick={onButtonClick}>
      Login
    </Button>

    {isLoading && <Loader />}

    {error && <Error message={error} />}
  </Wrapper>
);

export const Loader = () => <span>...</span>;

const Error = ({ message }) => <p>{message}</p>;

export default LoginForm;
