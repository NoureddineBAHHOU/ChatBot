import React from 'react';
import { withRouter } from 'react-router-dom';

import LoginForm from './LoginForm';
import AuthService from '../api/AuthService';

class LoginFormContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      phoneNumber: '',
      password: '',
      isLoading: false,
      error: false
    };
    this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.authService = new AuthService();
  }

  handlePhoneNumberChange(e) {
    this.setState({
      phoneNumber: e.target.value
    });
  }

  handlePasswordChange(e) {
    this.setState({
      password: e.target.value
    });
  }

  async handleButtonClick() {
    this.setState({ isLoading: true, error: '' });
    const { user, message } = await this.authService.login({
      phoneNumber: this.state.phoneNumber,
      password: this.state.password
    });
    this.setState({ isLoading: false });

    if (!user) {
      this.setState({ error: message });
    } else {
      this.props.history.push('/home');
    }
  }

  render() {
    return (
      <LoginForm
        phoneNumber={this.state.phoneNumber}
        password={this.state.password}
        isLoading={this.state.isLoading}
        error={this.state.error}
        onPhoneNumberChange={this.handlePhoneNumberChange}
        onPasswordChange={this.handlePasswordChange}
        onButtonClick={this.handleButtonClick}
      />
    );
  }
}

export default withRouter(LoginFormContainer);
