#!/usr/bin/env bash
npm i -g netlify-cli
netlify deploy --auth=$NETLIFY_AUTH --dir=build -m="$CI_COMMIT_TITLE" --prod --site=$1
